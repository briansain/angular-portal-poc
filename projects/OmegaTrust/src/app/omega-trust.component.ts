import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'omt-root',
  templateUrl: './omega-trust.component.html',
  styleUrls: ['./omega-trust.component.css']
})
export class OmegaTrustComponent implements OnInit {
  title = 'OmegaTrust';

  ngOnInit() {
    console.log('omega trust');
  }
}

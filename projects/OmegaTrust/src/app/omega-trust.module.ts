// tslint:disable:object-literal-shorthand
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OmegaTrustComponent } from './omega-trust.component';
import { HomeComponent } from './components/home/home.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';

const routes: Routes = [
  { path: 'home', component: OmegaTrustComponent },
  { path: 'omega-trust/home', pathMatch: 'full', component: OmegaTrustComponent },
];

const providers = [];
const declarations = [
  OmegaTrustComponent,
  HomeComponent
];

@NgModule({
  declarations: declarations,
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: providers,
  bootstrap: [OmegaTrustComponent]
})
export class OmegaTrustModule { }



@NgModule({
  declarations: declarations,
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class OmegaTrustSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OmegaTrustModule,
      providers: providers
    };
  }
}

import { TestBed, async } from '@angular/core/testing';
import { OmegaTrustComponent } from './omega-trust.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OmegaTrustComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(OmegaTrustComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'OmegaTrust'`, () => {
    const fixture = TestBed.createComponent(OmegaTrustComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('OmegaTrust');
  });

  // it('should render title in a h1 tag', () => {
  //   const fixture = TestBed.createComponent(OmegaTrustAppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to OmegaTrust!');
  // });
});

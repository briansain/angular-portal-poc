import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogData, ConfirmDialogComponent } from '@colonial/shared';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  sharedDialogResult: boolean;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openSharedDialog() {
    const dialogReference = this.dialog.open(ConfirmDialogComponent, {
      data: new ConfirmDialogData()
    });

    dialogReference.afterClosed().subscribe(result => {
      this.sharedDialogResult = result;
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VaultRoutingModule } from './vault-routing.module';
import { VaultHomeComponent } from './components/vault-home/vault-home.component';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [VaultHomeComponent],
  imports: [
    CommonModule,
    VaultRoutingModule,
    MatButtonModule
  ]
})
export class VaultModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VaultHomeComponent } from './components/vault-home/vault-home.component';


const routes: Routes = [
  {path: 'home', component: VaultHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VaultRoutingModule { }

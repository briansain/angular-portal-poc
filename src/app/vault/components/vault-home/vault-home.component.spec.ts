import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaultHomeComponent } from './vault-home.component';

describe('VaultHomeComponent', () => {
  let component: VaultHomeComponent;
  let fixture: ComponentFixture<VaultHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaultHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaultHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

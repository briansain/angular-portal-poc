import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogData, ConfirmDialogComponent } from '@colonial/shared';

@Component({
  selector: 'app-cdms-home',
  templateUrl: './cdms-home.component.html',
  styleUrls: ['./cdms-home.component.css']
})
export class CdmsHomeComponent implements OnInit {

  sharedDialogResult: boolean;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openSharedDialog() {
    const dialogReference = this.dialog.open(ConfirmDialogComponent, {
      data: { title: 'CDMS Dialog Title', message: 'Please don\'t click no' }
    });

    dialogReference.afterClosed().subscribe(result => {
      this.sharedDialogResult = result;
    });
  }
}

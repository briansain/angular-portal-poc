import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdmsHomeComponent } from './cdms-home.component';

describe('CdmsHomeComponent', () => {
  let component: CdmsHomeComponent;
  let fixture: ComponentFixture<CdmsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdmsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdmsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

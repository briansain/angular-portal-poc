import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CdmsHomeComponent } from './cdms-home/cdms-home.component';


const routes: Routes = [
  { path: 'home', component: CdmsHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CDMSRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material';
import { SharedModule } from '@colonial/shared';

import { CDMSRoutingModule } from './cdms-routing.module';
import { CdmsHomeComponent } from './cdms-home/cdms-home.component';


@NgModule({
  declarations: [CdmsHomeComponent],
  imports: [
    CommonModule,
    CDMSRoutingModule,
    MatButtonModule,
    SharedModule
  ]
})
export class CDMSModule { }
